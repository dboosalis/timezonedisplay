#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/icons/earth.svg"));
    a.setOrganizationName("spc");
    a.setApplicationName("timeZoneApp");
    a.setApplicationDisplayName("World Time Zones");
    MainWindow w;
    w.init();
    w.show();
    return a.exec();
}
