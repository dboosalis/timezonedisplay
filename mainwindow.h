#pragma once
#include <QMainWindow>
#include <QAction>
#include <QListView>
#include <QToolBar>
#include <QMenuBar>
#include <QQuickWidget>
#include <QPointer>
#include "controller.h"
#include "timezonemodel.h"
#include "timezoneselectiondialog.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void init();
protected slots:
    void showSelectionDialog();
    void closeSelectionDialog(int);
    void appAboutToQuit();
private:
    void buildActions();
    void buildMenusAndToolBars();
    QAction* m_exitA;
    QAction* m_configureA;
    QAction* m_clearA;
    QMenuBar* m_menuBar;
    QToolBar* m_toolBar;
    QListView* m_listView;
    TimeZoneModel* m_model;
    QQuickWidget* m_qmlView;
    Controller* m_controller;
    QMenu* m_fileMenu;
   QPointer<TimeZoneSelectionDialog> p_selectionDialog;

};
