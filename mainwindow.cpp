#include "mainwindow.h"
#include <QQmlContext>
#include <QGuiApplication>
#include <QSettings>
#include <QUrl>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    m_model = new TimeZoneModel(this);
    m_controller = new Controller(this);
    m_qmlView = new QQuickWidget(this);

    QQmlContext *context = m_qmlView->rootContext();
    context->setContextProperty("controller",m_controller);
    m_qmlView->setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_qmlView->setSource(QUrl("qrc:/qml/TimeZoneDisplayArea.qml"));
    setCentralWidget(m_qmlView);
    buildActions();
    buildMenusAndToolBars();
    connect(qApp,&QCoreApplication::aboutToQuit,this, &MainWindow::appAboutToQuit);

}

MainWindow::~MainWindow()
{
}
void MainWindow::init()
{
    QSettings settings(qApp->organizationName(),qApp->applicationName());
    QByteArray ba = settings.value("MainWindowState").toByteArray();
    if (ba.size() > 0) {
        restoreState(ba);
    }
    else {
        resize(900,900);
    }
    QRect rec = settings.value("MainWindowGeometry",QRect(100,100,900,900)).toRect();
    setGeometry(rec);

}
void MainWindow::buildActions()
{
    m_exitA = new QAction(QIcon(":/icons/logout.svg"),tr("E&xit"));
    connect(m_exitA,&QAction::triggered, this,[&] {
       qApp->exit();
    });
    m_configureA = new QAction(QIcon(":/icons/cog-outline.svg"),tr("@Configure"));
    m_configureA->setToolTip(tr("Need to create dialog to filter out timezones"));
    connect(m_configureA,&QAction::triggered, this,&MainWindow::showSelectionDialog);
}
void MainWindow::buildMenusAndToolBars()
{
    m_toolBar = addToolBar("toolbar");
    m_toolBar->addAction(m_exitA);

    m_toolBar->addAction(m_configureA);
    m_menuBar = menuBar();
    m_fileMenu = new QMenu("&File");
    m_fileMenu->addAction(m_configureA);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_exitA);
    m_menuBar->addMenu(m_fileMenu);


}
void  MainWindow::showSelectionDialog()
{
    if (!p_selectionDialog) {
        p_selectionDialog = new TimeZoneSelectionDialog();
        connect(p_selectionDialog,&QDialog::finished,this,&MainWindow::closeSelectionDialog);
        //* TODO check if pointers are null
        p_selectionDialog->setTimeZones(m_controller->timeZoneModel()->timeZones());
        QSettings settings(qApp->organizationName(),qApp->applicationName());
        //* TODO set default to center of current screen
        QRect rect = settings.value("SelectionDialogSize",QRect(100,100,800,800)).toRect();
        p_selectionDialog->setGeometry(rect);
    }
    p_selectionDialog->show();
    p_selectionDialog->raise();
}
void  MainWindow::closeSelectionDialog(int result)
{
    //* Finish by getting selection and apply to main window
    Q_UNUSED(result)
    QSettings settings(qApp->organizationName(),qApp->applicationName());
    settings.setValue("SelectionDialogSize", p_selectionDialog->geometry());
}
void MainWindow::appAboutToQuit()
{
    QSettings settings(qApp->organizationName(),qApp->applicationName());
    QByteArray ba = saveState();
    settings.setValue("MainWindowState",ba);
    settings.setValue("MainWindowGeometry",geometry());
}
