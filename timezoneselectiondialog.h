#pragma once

#include <QDialog>
#include <QAbstractListModel>
#include <QListView>
#include <QPushButton>
#include <QStandardItemModel>
#include <QTimeZone>
class TimeZoneSelectionDialog : public QDialog
{
    Q_OBJECT
public:
    TimeZoneSelectionDialog(QWidget* parent = nullptr);
    void setTimeZones(const QVector<QTimeZone> *);
    QListView* m_listView;
    QPushButton* m_okB;
    QPushButton* m_cancelB;
protected slots:
    void buttonPressed(QAbstractButton*);
private:
    QStandardItemModel* m_model;
};

