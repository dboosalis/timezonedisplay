#include "timezoneselectiondialog.h"
#include <QDebug>
#include <QDialogButtonBox>
#include <QLabel>
#include <QStandardItem>
#include <QVBoxLayout>

TimeZoneSelectionDialog::TimeZoneSelectionDialog(QWidget* parent): QDialog(parent)
{

    QVBoxLayout* vbox = new QVBoxLayout(this);
    setLayout(vbox);
    m_listView = new QListView(this);
    m_listView->setFlow(QListView::LeftToRight);
    m_listView->setWrapping(true);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(this);
    m_okB = buttonBox->addButton(QDialogButtonBox::QDialogButtonBox::Ok);
    m_cancelB =  buttonBox->addButton(QDialogButtonBox::QDialogButtonBox::Cancel);
    connect(buttonBox,&QDialogButtonBox::clicked, this,&TimeZoneSelectionDialog::buttonPressed);
    QLabel* titleL = new QLabel("Select Timezones");

    vbox->addWidget(titleL,0,Qt::AlignHCenter);
    vbox->addWidget(m_listView,1);
    vbox->addWidget(buttonBox,0);

    m_model = new QStandardItemModel(this);
    m_listView->setModel(m_model);

}
void TimeZoneSelectionDialog::buttonPressed(QAbstractButton* button)
{
    Q_UNUSED(button)
    qDebug() << "Button Pressed";
    if (button == m_cancelB)
        reject();
    else
        accept();
}
void TimeZoneSelectionDialog::setTimeZones(const QVector<QTimeZone> *timeZones)
{
    if (!timeZones) {
        qWarning() << "Time zone data is null " << __FILE__ << __LINE__;
    }
    for (auto tz : *timeZones) {
        if (tz.country() != QLocale::AnyCountry) {
            QStandardItem* item = new QStandardItem(tz.displayName(QDateTime::currentDateTime()));
            item->setCheckable(true);
            m_model->appendRow(item);
        }
    }
}
