import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.14

Rectangle {
    id: root
    anchors.fill: parent
    color: "#aaa"
    Component {
        id: tzDelegate
        Rectangle {
            width: listView.cellWidth
            height:  listView.cellHeight
            border.width: index === listView.currentIndex ? 2: 1
            border.color: index === listView.currentIndex ? "#eee" : "#555"
            color: index === listView.currentIndex ? "darkblue" : "#aaa"
            Label {
                id: _country
                text: country
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.leftMargin: 5
                anchors.left: parent.left
                anchors.right: _date.left
                anchors.rightMargin: 5
                elide: Text.ElideRight
                color: index === listView.currentIndex ? "white":"black"
            }
            Label {
                id:_date
                anchors.right: parent.right
                anchors.rightMargin: 5

                horizontalAlignment: Text.AlignRight
                anchors.topMargin: 5
                anchors.leftMargin: 5
                text: localDate
                color: index === listView.currentIndex ? "white":"black"

            }
            Label {
                id:_time
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignLeft
                font.pointSize: 32
                font.bold: true
                text: localTime
                color: index === listView.currentIndex ? "white":"black"

            }

            Label {
                id: _city
                text: displayName
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                anchors.leftMargin: 5
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.rightMargin: 5
                elide: Text.ElideRight
                color: index === listView.currentIndex ? "white":"black"
            }
            MouseArea {
                id: ma
                anchors.fill: parent
                onClicked: listView.currentIndex = index
            }
        }
    }

    GridView {
        id: listView
        model: controller.timeZoneModel
        anchors.fill: parent
        anchors.margins: {leftMargin:8;topMargin:5;rightMargin:5;bottomMargin:8}
        delegate: tzDelegate
        cellHeight: 120
        cellWidth: 220
    }
}
