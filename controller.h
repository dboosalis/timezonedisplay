#pragma once

#include <QObject>
#include "timezonemodel.h"

class Controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY (TimeZoneModel* timeZoneModel READ timeZoneModel CONSTANT)
public:
    explicit Controller(QObject *parent = nullptr);
    TimeZoneModel* timeZoneModel();
private:
    TimeZoneModel* m_timeZoneModel;

};

