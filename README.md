**TIME ZONE DISPLAY APPLICAION**
Okay this a Qt Widget application that uses QML for the listview.  Why ? Because it is much easier to draw and paint with :)
This application uses a model inherrited from QAbstactListItemModel and gets the time zones from Qt's QTimeZone class.

**NOT DONE**
Sorry I did not create the custom LCD widget for displaying the time, I already have 6 hrs into this application, and thing it would take another 4 to do this in QML. If I were to do this LCD as a Qt Widget I think it would take another 8 hours


* I recommend you start using some QML. It is much faster to get great a great custom look and feel then with widgets and QML  is hardware acelerated !

![Alt text](TimeZoneAppScreenShot.png?raw=true "Screen Shot")

---

## To Run
1. Load into QtCreator (Version 7.0 rc1 was used by me on Windows 11)
2. I used  Qt 5.15.2 on Wndows 11 with VS2019 Community edition.  I was unable to use Qt 6.3 prerelease.

Feel free to call me if have any questions -
David Boosalis, 312-859-7367 (cell)


-

