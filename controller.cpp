#include "controller.h"

Controller::Controller(QObject *parent)
    : QObject{parent}
{
    m_timeZoneModel = new TimeZoneModel(this);
}
TimeZoneModel* Controller::timeZoneModel()
{

   return  m_timeZoneModel;
}
