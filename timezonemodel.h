#pragma once

#include <QAbstractListModel>
#include <QString>
#include <QVector>
#include <QTimerEvent>
#include <QTimeZone>


/**
 * TODO Implment a QSortProxyFilter Model to allow one to show only selected items
*/
class TimeZoneModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit TimeZoneModel(QObject *parent = nullptr);
    enum RoleNames {
         DisplayNameRole     = Qt::UserRole,
         CountryRole     = Qt::UserRole + 1,
         LocalTimeRole   = Qt::UserRole + 2,
        LocalDateRole   = Qt::UserRole + 3,
       };

    const QVector<QTimeZone>* timeZones() const;
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
protected:
    void timerEvent(QTimerEvent *) final;
private:
     QHash<int, QByteArray> roleNames() const override;
     QVector<QTimeZone>* m_data;

};

