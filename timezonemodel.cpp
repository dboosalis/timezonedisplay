#include "timezonemodel.h"
#include <QByteArray>
#include <QDebug>
#include <QList>
#include <QLocale>

/* QTimeZone Data not a great choice, but good enough for now */

TimeZoneModel::TimeZoneModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_data = new QVector<QTimeZone>();
    QList<QByteArray> worldTimeZones = QTimeZone::availableTimeZoneIds();
    for (auto tz: worldTimeZones) {
        QTimeZone qtc = QTimeZone(tz);
        QDateTime dt(QDate::currentDate(),QTime::currentTime(),qtc);
        if ((qtc.country() != QLocale::AnyCountry) &&
                // polutes data with to many 'Greenwich Standard Time'"
                (qtc.displayName(QDateTime::currentDateTime()) != "Greenwich Standard Time")) {
            m_data->push_back(qtc);
        }
    }
    startTimer(1000);

}

int TimeZoneModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;
    if (!m_data)
        return 0;

    return m_data->size();
}

QVariant TimeZoneModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (!m_data)
        return QVariant();

    if (index.row() < 0 || index.row() >= m_data->count())
    {
        qWarning() << "\tOut of range " << __FILE__ << __LINE__;
        return QVariant();
    }
    QTimeZone tz = (*m_data)[index.row()];

    switch(role) {

    case  DisplayNameRole:
        return tz.displayName(QDateTime::currentDateTime());
    case CountryRole:
        return  QLocale::countryToString(tz.country());
    case LocalTimeRole:
        return QDateTime(QDate::currentDate(),QTime::currentTime(),tz).toLocalTime().toString("HH:mm");
    case LocalDateRole:
        return QDateTime(QDate::currentDate(),QTime::currentTime(),tz).toLocalTime().toString("MMM dd");
    default:
        qWarning() << "Unknown role sought for index = " << index << __FILE__ << __LINE__;
    }

    return QVariant();
}

bool TimeZoneModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
    return true;
}

bool TimeZoneModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
    return true;
}
QHash<int, QByteArray> TimeZoneModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DisplayNameRole] = "displayName";
    roles[CountryRole] = "country";
    roles[LocalTimeRole] = "localTime";
    roles[LocalDateRole] = "localDate";


    return roles;
}
void TimeZoneModel::timerEvent(QTimerEvent *te)
{
    Q_UNUSED(te);
    QModelIndex firstIndex = index(0);
    QModelIndex lastIndex  = index(m_data->size()-1);
    QVector<int> changedRoles;
    changedRoles.append(LocalTimeRole);
    changedRoles.append(LocalDateRole);
    emit dataChanged(firstIndex,lastIndex,changedRoles);

}
const QVector<QTimeZone>* TimeZoneModel::timeZones() const
{
    return m_data;
}

